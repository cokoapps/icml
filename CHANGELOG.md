## [2.0.2](https://gitlab.coko.foundation/cokoapps/icml/compare/v2.0.1...v2.0.2) (2025-02-04)


### Bug Fixes

* fix postgres ssl connections ([8c7bd77](https://gitlab.coko.foundation/cokoapps/icml/commit/8c7bd770873d0db2374c88c6044f8a9d52089f9c))

## [2.0.1](https://gitlab.coko.foundation/cokoapps/icml/compare/v2.0.0...v2.0.1) (2024-12-13)


### Bug Fixes

* create client from env at startup ([6946cd5](https://gitlab.coko.foundation/cokoapps/icml/commit/6946cd525d3cd715622bc9a2611247f612536115))

# [2.0.0](https://gitlab.coko.foundation/cokoapps/icml/compare/v1.2.2...v2.0.0) (2024-12-09)


### chore

* upgrade to coko server v4 ([a39f84d](https://gitlab.coko.foundation/cokoapps/icml/commit/a39f84d4c96795bfc75edbb711240ca65e618153))


### BREAKING CHANGES

* environment variables: renamed PUBSWEET_SECRET to SECRET, dropped SERVER_PROTOCOL and SERVER_HOST

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.2.2](https://gitlab.coko.foundation/cokoapps/icml/compare/v1.2.1...v1.2.2) (2024-02-01)

### [1.2.1](https://gitlab.coko.foundation/cokoapps/icml/compare/v1.2.0...v1.2.1) (2023-02-10)

## [1.2.0](https://gitlab.coko.foundation/cokoapps/icml/compare/v1.1.0...v1.2.0) (2022-10-13)


### Features

* **service:** node 16 ([54c4c4f](https://gitlab.coko.foundation/cokoapps/icml/commit/54c4c4fd7122754d7e2e83e91f941ecd428f34a4))


### Bug Fixes

* go back to a older image which work on m1 ([99b6028](https://gitlab.coko.foundation/cokoapps/icml/commit/99b60288eb7b8fac2eeed643a849867aa32c2d4a))

## 1.1.0 (2021-04-15)


### Features

* **service:** service for converting html to icml finalized ([c424950](https://gitlab.coko.foundation/cokoapps/icml/commit/c424950c15ed70badb311d7281f8e3a54630c67e))
* **service:** tmp file clean-up ([decdc18](https://gitlab.coko.foundation/cokoapps/icml/commit/decdc18e7af3529121c2dae0731b8966ae8848ca))
