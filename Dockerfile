FROM node:20-bookworm-slim

RUN corepack enable
RUN apt-get update && apt-get install -y pandoc

WORKDIR /home/node/icml

RUN chown -R node:node /home/node/icml
USER node

COPY --chown=node:node .yarnrc.yml .
COPY --chown=node:node package.json ./package.json
COPY --chown=node:node yarn.lock ./yarn.lock

RUN yarn workspaces focus --production && yarn cache clean && rm -rf ~/.npm

COPY --chown=node:node . .

CMD ["yarn", "coko-server", "start"]
